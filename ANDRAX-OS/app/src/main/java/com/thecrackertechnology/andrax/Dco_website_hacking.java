package com.thecrackertechnology.andrax;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;

import com.thecrackertechnology.dragonterminal.bridge.Bridge;

public class Dco_website_hacking extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(savedInstanceState);


        setContentView(R.layout.dco_website_hacking);

        CardView cardviewodin = findViewById(R.id.card_view_odin);
        CardView cardviewdotdotpwn = findViewById(R.id.card_view_dotdotpwn);
        CardView cardviewnodexp = findViewById(R.id.card_view_nodexp);
        CardView cardviewxxeenum = findViewById(R.id.card_view_xxe_enum);
        CardView cardviewxxeinjector = findViewById(R.id.card_view_xxeinjector);
        CardView cardviewxxexploiter = findViewById(R.id.card_view_xxexploiter);
        CardView cardviewxxetimes = findViewById(R.id.card_view_xxetimes);
        CardView cardviewmitmproxy = findViewById(R.id.card_view_mitmproxy);
        CardView cardviewzap = findViewById(R.id.card_view_zap);
        CardView cardviewhttptools = findViewById(R.id.card_view_httptools);
        CardView cardviewwapiti = findViewById(R.id.card_view_wapiti);
        CardView cardviewreconng = findViewById(R.id.card_view_reconng);
        CardView cardviewcloudsploit = findViewById(R.id.card_view_cloudsploit);
        CardView cardviewphpsploit = findViewById(R.id.card_view_phpsploit);
        CardView cardviewxsstrike = findViewById(R.id.card_view_xsstrike);
        CardView cardviewphoton = findViewById(R.id.card_view_photon);
        CardView cardviewxsser = findViewById(R.id.card_view_xsser);
        CardView cardviewcommix = findViewById(R.id.card_view_commix);
        CardView cardviewsqlmap = findViewById(R.id.card_view_sqlmap);
        CardView cardviewbbqsql = findViewById(R.id.card_view_bbqsql);
        CardView cardviewpayloadmask = findViewById(R.id.card_view_payloadmask);
        CardView cardviewabernathy = findViewById(R.id.card_view_abernathy);
        CardView cardviewclusterd = findViewById(R.id.card_view_clusterd);
        CardView cardviewdirb = findViewById(R.id.card_view_dirb);
        CardView cardviewhamster = findViewById(R.id.card_view_hamster);
        CardView cardviewhttrack = findViewById(R.id.card_view_httrack);
        CardView cardviewarjun = findViewById(R.id.card_view_arjun);
        CardView cardviewput2win = findViewById(R.id.card_view_put2win);
        CardView cardviewwafninja = findViewById(R.id.card_view_wafninja);
        CardView cardviewxsrfprobe = findViewById(R.id.card_view_xsrfprobe);
        CardView cardviewevilurl = findViewById(R.id.card_view_evilurl);
        CardView cardviewcrlfinjector = findViewById(R.id.card_view_crlfinjector);
        CardView cardviewinjectus = findViewById(R.id.card_view_injectus);
        CardView cardviewfiesta = findViewById(R.id.card_view_fiesta);
        CardView cardviewwhatweb = findViewById(R.id.card_view_whatweb);
        CardView cardviewgolismero = findViewById(R.id.card_view_golismero);
        CardView cardviewwafw00f = findViewById(R.id.card_view_wafw00f);
        CardView cardviewjaeles = findViewById(R.id.card_view_jaeles);
        CardView cardviewnuclei = findViewById(R.id.card_view_nuclei);
        CardView cardviewhttpx = findViewById(R.id.card_view_httpx);
        CardView cardviewnikto = findViewById(R.id.card_view_nikto);
        CardView cardviewuniscan = findViewById(R.id.card_view_uniscan);
        CardView cardviewsitebroker = findViewById(R.id.card_view_sitebroker);
        CardView cardviewpyfilebuster = findViewById(R.id.card_view_pyfilebuster);
        CardView cardviewadfind = findViewById(R.id.card_view_adfind);
        CardView cardviewwpxf = findViewById(R.id.card_view_wpxf);
        CardView cardviewwpscan = findViewById(R.id.card_view_wpscan);
        CardView cardviewjoomlavs = findViewById(R.id.card_view_joomlavs);
        CardView cardviewwpseku = findViewById(R.id.card_view_wpseku);
        CardView cardviewcmseek = findViewById(R.id.card_view_cmseek);
        CardView cardviewwascan = findViewById(R.id.card_view_wascan);
        CardView cardviewaron = findViewById(R.id.card_view_aron);
        CardView cardviewjwtcrack = findViewById(R.id.card_view_jwtcrack);
        CardView cardviewjwt_tool = findViewById(R.id.card_view_jwt_tool);
        CardView cardviewhhh = findViewById(R.id.card_view_hhh);
        CardView cardviewhsecscan = findViewById(R.id.card_view_hsecscan);
        CardView cardviewxanxss = findViewById(R.id.card_view_xanxss);
        CardView cardviewwfuzz = findViewById(R.id.card_view_wfuzz);
        CardView cardviewvulnx = findViewById(R.id.card_view_vulnx);
        CardView cardviewfingerprinter = findViewById(R.id.card_view_fingerprinter);
        CardView cardviewuatester = findViewById(R.id.card_view_uatester);
        CardView cardviewcadaver = findViewById(R.id.card_view_cadaver);
        CardView cardviewxspear = findViewById(R.id.card_view_xspear);
        CardView cardviewimagejs = findViewById(R.id.card_view_imagejs);
        CardView cardviewwpforce = findViewById(R.id.card_view_wpforce);
        CardView cardviewyertle = findViewById(R.id.card_view_yertle);

        cardviewodin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("0d1n");

            }
        });

        cardviewdotdotpwn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("dotdotpwn");

            }
        });

        cardviewmitmproxy.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("mitmproxy");

            }
        });

        cardviewzap.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("zap");

            }
        });

        cardviewhttptools.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("httptools -h");

            }
        });

        cardviewwapiti.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wapiti");

            }
        });

        cardviewreconng.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("recon-ng");

            }
        });

        cardviewcloudsploit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("cloudsploit");

            }
        });

        /**
         *
         * Help me, i'm dying...
         *
         **/

        cardviewphpsploit.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("phpsploit");

            }
        });

        cardviewxsstrike.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("xsstrike");

            }
        });

        cardviewphoton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("photon");

            }
        });

        cardviewxsser.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("xsser");

            }
        });

        cardviewcommix.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("commix");

            }
        });

        cardviewsqlmap.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sqlmap");

            }
        });

        cardviewbbqsql.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("bbqsql");

            }
        });

        cardviewpayloadmask.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("payloadmask");

            }
        });

        cardviewabernathy.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("abernathy");

            }
        });

        cardviewclusterd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("clusterd");

            }
        });

        cardviewdirb.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("dirb");

            }
        });

        cardviewhamster.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("hamster");

            }
        });

        cardviewhttrack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("httrack");

            }
        });


        cardviewarjun.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("arjun -h");

            }
        });

        cardviewput2win.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("put2win -h");

            }
        });

        cardviewwafninja.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wafninja -h");

            }
        });

        cardviewxsrfprobe.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("xsrfprobe");

            }
        });

        cardviewevilurl.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("evilurl");

            }
        });

        cardviewcrlfinjector.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("crlf-injector -h");

            }
        });

        cardviewinjectus.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("injectus");

            }
        });

        cardviewfiesta.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("fiesta -h");

            }
        });

        cardviewwhatweb.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("whatweb -h");

            }
        });

        cardviewgolismero.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("golismero --help");

            }
        });

        cardviewwafw00f.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wafw00f -h");

            }
        });

        cardviewjaeles.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("jaeles");

            }
        });

        cardviewnuclei.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("nuclei");

            }
        });

        cardviewhttpx.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("httpx");

            }
        });

        cardviewnikto.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("nikto");

            }
        });

        cardviewuniscan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sudo uniscan");

            }
        });

        cardviewsitebroker.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sitebroker");

            }
        });

        cardviewpyfilebuster.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("filebuster -h");

            }
        });

        cardviewadfind.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("adfind -h");

            }
        });

        cardviewwpxf.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wpxf");

            }
        });

        cardviewwpscan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wpscan -h");

            }
        });

        cardviewjoomlavs.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("joomlavs");

            }
        });

        cardviewvulnx.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("vulnx -h");

            }
        });

        cardviewfingerprinter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("fingerprinter -h");

            }
        });

        cardviewwpseku.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wpseku -h");

            }
        });

        cardviewcmseek.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("sudo cmseek");

            }
        });

        cardviewwascan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wascan");

            }
        });

        cardviewaron.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("aron -h");

            }
        });

        cardviewjwtcrack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("jwtcrack");

            }
        });

        cardviewjwt_tool.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("jwt_tool -h");

            }
        });

        cardviewhhh.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("hhh -h");

            }
        });

        cardviewhsecscan.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("hsecscan");

            }
        });

        cardviewnodexp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("nodexp --help");

            }
        });

        cardviewxxeenum.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("xxe-enum-client -h");

            }
        });

        cardviewxxeinjector.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("xxeinjector");

            }
        });

        cardviewxxexploiter.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("xxexploiter");

            }
        });

        cardviewxxetimes.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("xxetimes -h");

            }
        });

        cardviewxanxss.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("xanxss -h");

            }
        });

        cardviewwfuzz.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wfuzz --help");

            }
        });

        cardviewuatester.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("ua-tester");

            }
        });

        cardviewcadaver.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("cadaver");

            }
        });

        cardviewxspear.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("XSpear -h");

            }
        });

        cardviewimagejs.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("imagejs");

            }
        });

        cardviewwpforce.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("wpforce -h");

            }
        });

        cardviewyertle.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                run_hack_cmd("yertle -h");

            }
        });


    }

    public void run_hack_cmd(String cmd) {

        Intent intent = Bridge.createExecuteIntent(cmd);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);

    }

    @Override
    public void onPause() {

        super.onPause();
        finish();
    }
}
